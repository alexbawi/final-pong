/*******************************************************************************************
*
*   raylib game: FINAL PONG - game template
*
*   developed by [Alex Barceló Wiemeyer]
*
*   This example has been created using raylib 1.0 (www.raylib.com)
*   raylib is licensed under an unmodified zlib/libpng license (View raylib.h for details)
*
*   Copyright (c) 2014 Ramon Santamaria (Ray San)
*
********************************************************************************************/

#include "raylib.h"

typedef enum GameScreen { LOGO, TITLE, GAMEPLAY, ENDING } GameScreen;

int main()
{
    // Initialization
    //--------------------------------------------------------------------------------------
    int screenWidth = 800;
    int screenHeight = 450;
    char windowTitle[30] = "raylib game - FINAL PONG";
    
    GameScreen screen = LOGO;
    
    // TODO: Define required variables here..........................(0.5p)        
    const int velocidady = 8;
    const int ballSize = 25;
    const int maxVelocity = 20;
    const int minVelocity = 8;
   
    char *texto;
    Vector2 tamanoTexto;
   
    bool pause = false;
   
    int score1p = 0;
    int score2p = 0;
   
    Rectangle paladerecha;
   
    paladerecha.width = 20;
    paladerecha.height = 100;    
    paladerecha.x = screenWidth - 50 - paladerecha.width;
    paladerecha.y = screenHeight/2 - paladerecha.height/2;
   
   
    Rectangle palaizquierda;
   
    palaizquierda.width = 20;
    palaizquierda.height = 100;    
    palaizquierda.x = 50;
    palaizquierda.y = screenHeight/2 - palaizquierda.height/2;
   
   
    Vector2 ball;
    ball.x = screenWidth/2;
    ball.y = screenHeight/2;
   
    Vector2 ballVelocity;
    ballVelocity.x = minVelocity;
    ballVelocity.y = minVelocity;
     Font fontTtf = LoadFontEx("Resources/Stranger back in the Night.ttf", 32, 0, 250);
   
    
    // NOTE: Here there are some useful variables (should be initialized)
    Rectangle player;
    int playerSpeedY;
    
    Rectangle enemy;
    int enemySpeedY;
    
    Vector2 ballPosition;
    Vector2 ballSpeed;
    int ballRadius;
    
    int playerLife;
    int enemyLife;
    
    int secondsCounter = 99;
    
    int framesCounter;          // General pourpose frames counter
    
    int gameResult = -1;        // 0 - Loose, 1 - Win, -1 - Not defined
    
    InitWindow(screenWidth, screenHeight, windowTitle);
    
    // NOTE: If using textures, declare Texture2D variables here (after InitWindow)
    // NOTE: If using SpriteFonts, declare SpriteFont variables here (after InitWindow)
    
    // NOTE: If using sound or music, InitAudioDevice() and load Sound variables here (after InitAudioDevice)
    
    SetTargetFPS(60);
    //--------------------------------------------------------------------------------------
    
    // Main game loop
    while (!WindowShouldClose())    // Detect window close button or ESC key
    {
        // Update
        //----------------------------------------------------------------------------------
        
       if(!pause){
            if (IsKeyDown(KEY_Q)){
              palaizquierda.y -= velocidady;
            }
           
            if (IsKeyDown(KEY_A)){
              palaizquierda.y += velocidady;
            }
           
            if (IsKeyDown(KEY_UP)){
              paladerecha.y -= velocidady;
            }
           
            if (IsKeyDown(KEY_DOWN)){
              paladerecha.y += velocidady;
            }
        }
       
        if (IsKeyPressed(KEY_P)){
          pause = !pause;
        }
       
        //Consulto Limites
        if(palaizquierda.y<0){
            palaizquierda.y = 0;
        }
       
        if(palaizquierda.y > (screenHeight - palaizquierda.height)){
            palaizquierda.y = screenHeight - palaizquierda.height;
        }
       
        if(paladerecha.y<0){
            paladerecha.y = 0;
        }
       
        if(paladerecha.y > (screenHeight - paladerecha.height)){
            paladerecha.y = screenHeight - paladerecha.height;
        }
       
        //Gestionamos el movimiento de la Bola
        if(!pause){
            ball.x += ballVelocity.x;
            ball.y += ballVelocity.y;
        }
       
        //Aqui marcamos gol
        if(ball.x > screenWidth - ballSize){
            //Marca la pala izquierda
            //PlaySound(fxGoal);
            score1p++;
            ball.x = screenWidth/2;
            ball.y = screenHeight/2;
            ballVelocity.x = -minVelocity;
            ballVelocity.y = minVelocity;
           
        }else if(ball.x < ballSize){
            //Marca la pala derecha
            //PlaySound(fxGoal);
            score2p++;
            ball.x = screenWidth/2;
            ball.y = screenHeight/2;
            ballVelocity.x = minVelocity;
            ballVelocity.y = minVelocity;
        }
       
        if((ball.y > screenHeight - ballSize) || (ball.y < ballSize) ){
            ballVelocity.y *=-1;
            //PlaySound(fxWav);
        }
 
 
        if(CheckCollisionCircleRec(ball, ballSize, paladerecha)){
            if(ballVelocity.x>0){
                //PlaySound(fxWav);
                if(abs(ballVelocity.x)<maxVelocity){                    
                    ballVelocity.x *=-1.5;
                    ballVelocity.y *= 1.5;
                }else{
                    ballVelocity.x *=-1;
                }
            }
        }
       
        if(CheckCollisionCircleRec(ball, ballSize, palaizquierda)){
            if(ballVelocity.x<0){
                //PlaySound(fxWav);
                if(abs(ballVelocity.x)<maxVelocity){                    
                    ballVelocity.x *=-1.5;
                    ballVelocity.y *= 1.5;
                }else{
                    ballVelocity.x *=-1;
                }
            }
        }
       
        //----------------------------------------------------------------------------------
 
        // Draw
        //----------------------------------------------------------------------------------
        BeginDrawing();
 
            ClearBackground(BLACK);
           
            // Pinto Pala Derecha
            DrawRectangleRec(paladerecha, BLUE);
           
            // Pinto Pala Izquierda
            DrawRectangleRec(palaizquierda, BLUE);
           
            // Pintamos la pelota
            DrawCircleV(ball, ballSize, BLUE);  
           
            // Texto ejemplo
           
            texto = FormatText("SCORE 1P: %d",score1p);
           
            DrawTextEx(fontTtf, texto, (Vector2){0,10}, 40, 0, PINK);
 
            texto = FormatText("SCORE 2P: %d",score2p);
            tamanoTexto = MeasureTextEx(fontTtf, texto, 40, 0);
           
            DrawTextEx(fontTtf, texto, (Vector2){screenWidth - tamanoTexto.x,10}, 40, 0, PINK);
            //DrawText(FormatText("SCORE 1P: %d",score1p), 10, 10, 40, RED);
            //DrawText(FormatText("SCORE 2P: %d",score2p), screenWidth - 300, 10, 40, RED);
           
        
        switch(screen) 
        {
            case LOGO: 
            {
                // Update LOGO screen data here!
                
                // TODO: Logo fadeIn and fadeOut logic...............(0.5p)
                
            } break;
            case TITLE: 
            {
                // Update TITLE screen data here!
                
                // TODO: Title animation logic.......................(0.5p)
                
                // TODO: "PRESS ENTER" logic.........................(0.5p)
                
            } break;
            case GAMEPLAY:
            { 
                // Update GAMEPLAY screen data here!

                // TODO: Ball movement logic.........................(0.2p)
                
                // TODO: Player movement logic.......................(0.2p)
                
                // TODO: Enemy movement logic (IA)...................(1p)
                
                // TODO: Collision detection (ball-player) logic.....(0.5p)
                
                // TODO: Collision detection (ball-enemy) logic......(0.5p)
                
                // TODO: Collision detection (ball-limits) logic.....(1p)
                
                // TODO: Life bars decrease logic....................(1p)

                // TODO: Time counter logic..........................(0.2p)

                // TODO: Game ending logic...........................(0.2p)
                
                // TODO: Pause button logic..........................(0.2p)
                
            } break;
            case ENDING: 
            {
                // Update END screen data here!
                
                // TODO: Replay / Exit game logic....................(0.5p)
                
            } break;
            default: break;
        }
        //----------------------------------------------------------------------------------
        
        // Draw
        //----------------------------------------------------------------------------------
        BeginDrawing();
        
            ClearBackground(RAYWHITE);
            
            switch(screen) 
            {
                case LOGO: 
                {
                    // Draw LOGO screen here!                    
                    DrawText("PONG", screenWidth/2 - MeasureText("PONG", 40)/2 , screenHeight/2, 40, BLUE);
                    
                    // TODO: Draw Logo...............................(0.2p)
                    
                } break;
                case TITLE: 
                {
                    // Draw TITLE screen here!
                    
                    // TODO: Draw Title..............................(0.2p)
                    
                    // TODO: Draw "PRESS ENTER" message..............(0.2p)
                    
                } break;
                case GAMEPLAY:
                { 
                    // Draw GAMEPLAY screen here!
                    
                    // TODO: Draw player and enemy...................(0.2p)
                    
                    // TODO: Draw player and enemy life bars.........(0.5p)
                    
                    // TODO: Draw time counter.......................(0.5p)
                    
                    // TODO: Draw pause message when required........(0.5p)
                    
                } break;
                case ENDING: 
                {
                    // Draw END screen here!
                    
                    // TODO: Draw ending message (win or loose)......(0.2p)
                    
                } break;
                default: break;
            }
        
            DrawFPS(10, 10);
            
            if(pause){
                DrawRectangle(0, 0, screenWidth, screenHeight, (Color){ 0, 255, 0, 255/2 });  
                DrawText("Press p to continue", screenWidth/2 - MeasureText("Press p to continue", 40)/2 , screenHeight/2, 40, PINK);
            }
        
        EndDrawing();
        //----------------------------------------------------------------------------------
    }

    // De-Initialization
    //--------------------------------------------------------------------------------------
    
    // NOTE: Unload any Texture2D or SpriteFont loaded here
    
    CloseWindow();        // Close window and OpenGL context
    //--------------------------------------------------------------------------------------
    
    return 0;
}